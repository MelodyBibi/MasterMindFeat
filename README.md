Il s'agit de réaliser un MasterMind versus, l'ordinateur génère une combinaison aléatoire et chaque joueur a pour but de trouver la combinaison avant l'autre.
La combinaison sera composé de 4 éléments et plus en fonction de la difficulté (en option).
Un joueur est désigné pour essayer de trouver sa combinaison pendant un nombre de tour à définir et pendant ce temps l'autre joueur sèmera d'embûches 
la tentative de l'autre joueur (par exemple inverser deux pions de place ou cacher une partie de la réponse de l'ordinateur).
Si le premier joueur n'as pas trouvé pendant le nombre de tour imparti, on inverse les positions et cela jusqu'à ce qu'un des deux trouve sa combinaison.


- 2 joueurs : si 0, 1 ou + 2 joueurs --> Pas de partie
- 4 elements
- 7 couleurs



Règles :
- Défense : Une fois que l'attaquant à soumis sa proposition, le défenseur peut intervertir 2 couleurs et la proposition puis soumise à la validation
- 1234
- position + couleur trouvée : noir
- couleur : blanc


