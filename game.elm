import Html exposing (Html, fieldset, input, label, text)
import Html.App as App
import Html.Attributes exposing (style, type')
import Html.Events exposing (onClick)
import Html exposing (Html)
import Html exposing (img)
import Html exposing (..)
import Html.Attributes exposing (..)

main =
  App.beginnerProgram { model = optOut, update = update, view = view }



-- MODEL

type alias Color =
  {clicked : Bool
  , required : Bool
  , path : String
  }

azure =
  {clicked = False
  , required = True
  , path = "assets/azure.png"
  }

blue =
  {clicked = False
  , required = True
  , path = "assets/blue.png"
  }

grey =
  {clicked = False
  , required = True
  , path = "assets/grey.png"
  }

green =
  {clicked = False
  , required = True
  , path = "assets/green.png"
  }

orange =
  {clicked = False
  , required = False
  , path = "assets/orange.png"
  }

red =
  {clicked = False
  , required = False
  , path = "assets/red.png"
  }

pink =
  {clicked = False
  , required = False
  , path = "assets/pink.png"
  }

type alias Model =
  { azure : Color
  , blue : Color
  , grey : Color
  , green : Color
  , orange : Color
  , pink : Color
  , red : Color
  , test : String
  }

optOut : Model
optOut =
  Model azure blue grey green orange pink red "test"



-- UPDATE


type Msg
  = Togglecolor1
  | Togglecolor2
  | Togglecolor3
  | Togglecolor4
  | Togglecolor5
  | Togglecolor6
  | Togglecolor7
  | Valider


update : Msg -> Model -> Model
update msg model =
  case msg of
    Togglecolor1 ->
      { model | azure = {clicked = not model.azure.clicked, required = model.azure.required, path = model.azure.path } }

    Togglecolor2 ->
      { model | blue = {clicked = not model.blue.clicked, required = model.blue.required, path = model.blue.path } }

    Togglecolor3 ->
      { model | grey = {clicked = not model.grey.clicked, required = model.grey.required, path = model.grey.path } }

    Togglecolor4 ->
      { model | green = {clicked = not model.green.clicked, required = model.green.required, path = model.green.path } }

    Togglecolor5 ->
      { model | orange = {clicked = not model.orange.clicked, required = model.orange.required, path = model.orange.path } }

    Togglecolor6 ->
      { model | pink = {clicked = not model.pink.clicked, required = model.pink.required, path = model.pink.path } }

    Togglecolor7 ->
      { model | red = {clicked = not model.red.clicked, required = model.red.required, path = model.red.path } }

    Valider ->
      { model | test = 
                  if model.azure.clicked == model.azure.required 
                    && model.blue.clicked == model.blue.required 
                    && model.grey.clicked == model.grey.required 
                    && model.green.clicked == model.green.required 
                    && model.orange.clicked == model.orange.required
                    && model.pink.clicked == model.pink.required  
                    && model.red.clicked == model.red.required 
                  then "Win" 
                  else "Loose"}

-- VIEW

size : Int 
size = 30

view : Model -> Html Msg 
view model = 
  div []
    [
      img
        [ src model.azure.path
        , width size
        , height size
        ] [ ]
      ,img
        [ src model.blue.path
        , width size
        , height size
        ] [ ]
      ,img
        [ src model.green.path
        , width size
        , height size
        ] [ ]
      ,img
        [ src model.grey.path
        , width size
        , height size
        ] [ ]
      ,fieldset []
        [ checkbox Togglecolor1 "Azure"
        , checkbox Togglecolor2 "Blue"
        , checkbox Togglecolor3 "Green"
        , checkbox Togglecolor4 "Grey"
        , checkbox Togglecolor5 "Orange"
        , checkbox Togglecolor6 "Pink"
        , checkbox Togglecolor7 "Red"
        ]
      , button [ onClick Valider ] [ text "Valider" ] 
      , text (toString model.test)
    ]


checkbox : msg -> String -> Html msg
checkbox msg name =
  label
    [ style [("padding", "20px")]
    ]
    [ input [ type' "checkbox", onClick msg ] []
    , text name
    ]
